import React from "react";
// react plugin used to create charts
import { Doughnut } from "react-chartjs-2";
// react plugin for creating vector maps
import { VectorMap } from "react-jvectormap";

// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Table,
  Row,
  Col
} from "reactstrap";

import {
  chartExample5,
  chartExample6,
  chartExample7
} from "variables/charts.jsx";

//import Api thống kê
import Thongke from "../Api/Thongke/Thongke";

var mapData = {
  AU: 760,
  BR: 550,
  CA: 120,
  DE: 1300,
  FR: 540,
  GB: 690,
  GE: 200,
  IN: 200,
  RO: 600,
  RU: 300,
  US: 2920
};

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nguoidung: [],
      benhvien: [],
      tintuc: []
    };
  }
  componentDidMount() {
    Thongke()
      .then(resJson => {
        this.setState({
          nguoidung: resJson.nguoidung,
          benhvien: resJson.benhvien,
          tintuc: resJson.tintuc
        });
      })
      .catch(err => console.log(err));
  }
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="fas fa-user text-info" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        {this.state.nguoidung.map((item, index) => (
                          <CardTitle tag="p">{item.soluongnguoidung}</CardTitle>
                        ))}
                        <p className="card-category">Người dùng</p>
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="fa fa-refresh" />
                    Cập nhật
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="fas fa-hospital text-success" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        {this.state.benhvien.map((item, index) => (
                          <CardTitle tag="p">{item.dsbenhvien}</CardTitle>
                        ))}
                        <p className="card-category">Bệnh viện</p>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="fa fa-refresh" />
                    Cập nhật
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="far fa-newspaper text-danger" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        {this.state.tintuc.map((item, index) => (
                          <CardTitle tag="p">{item.dstintuc}</CardTitle>
                        ))}
                        <p className="card-category">Tin tức</p>

                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="fa fa-refresh" />
                    Update now
                  </div>
                </CardFooter>
              </Card>
            </Col>
            <Col lg="3" md="6" sm="6">
              <Card className="card-stats">
                <CardBody>
                  <Row>
                    <Col md="4" xs="5">
                      <div className="icon-big text-center icon-warning">
                        <i className="fas fa-users text-primary" />
                      </div>
                    </Col>
                    <Col md="8" xs="7">
                      <div className="numbers">
                        <CardTitle tag="p">7910</CardTitle>
                        <p className="card-category">Xét nghiệm</p>
                        <p />
                      </div>
                    </Col>
                  </Row>
                </CardBody>
                <CardFooter>
                  <hr />
                  <div className="stats">
                    <i className="fa fa-refresh" />
                   Cập nhật
                  </div>
                </CardFooter>
              </Card>
            </Col>
          </Row>

          <Row />
        </div>
      </>
    );
  }
}

export default Dashboard;

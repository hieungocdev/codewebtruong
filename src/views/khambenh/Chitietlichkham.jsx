import React from "react";

// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  Form,
  UncontrolledTooltip
} from "reactstrap";

class Chitietlichkham extends React.Component {
    // phuong thuc khoi tao
    constructor(props) {
      super(props);
      this.state = {
        tentintuc: "",
        hinhanh: "",
        noidung:"",
        tentintucState: "",
        hinhanhState: "",
        noidungState:"",
        alert_message: "",
        data:[],
        visible: true
      };
    }
  // lay thong tin chi tiet benh
fetchData = async() =>{
  const { match: { params } } = this.props;  
  const response =  await fetch(`http://45.76.198.77/apiedoctor/admin/khambenh/chitietlich.php?id=${params.id}`);
  const products = await response.json(); 
  this.setState({data: products}); 
};
componentDidMount(){
  this.fetchData();
}
  render() {
    return (
      <>
        <div className="content">
          <center><h2 class="title">Quản lý lịch khám</h2></center>
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
              <center><h5 className="title">Thông tin lịch khám</h5></center>    
                </CardHeader>
                {
                  this.state.data.map((item, index)=>(
                    <CardBody>
                  <Form>
                    <Row>
                      <Col className="pr-1" md="7">
                        <FormGroup>
                        <label>Họ và tên</label>
                          <Input
                          disabled
                            defaultValue={item.hovaten}
                            placeholder="Company"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-1" md="3">
                        <FormGroup>
                        <label>Ngày sinh</label>
                        <input type="text" disabled class="form-control datetimepicker" value={item.ngaysinh}/>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="5">
                        <FormGroup>
                        <label>Giới tính</label>
                          <Input
                          disabled
                            defaultValue={item.gioitinh}
                            placeholder="Company"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-1" md="5">
                        <FormGroup>
                        <label>SĐT</label>
                        <Input
                        disabled
                            defaultValue={item.sodienthoai}
                            placeholder="Company"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="6">
                        <FormGroup>
                        <label>Tài khoản</label>
                          <Input
                            defaultValue={item.email}
                            disabled
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-1" md="6">
                        <FormGroup>
                        <label htmlFor="exampleInputEmail1">
                            Email
                          </label>
                          <Input disabled value={item.email} type="email" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Địa chỉ</label>
                          <Input
                          disabled
                            defaultValue={item.diachi}
                          
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="6">
                        <FormGroup>
                          <label>Nghề nghiệp</label>
                          <Input
                          disabled
                            defaultValue={item.nghenghiep}
                            placeholder="City"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                    <Col className="pr-1" md="3">
                        <FormGroup>
                        <label>Ngày giờ đặt lịch</label>
                        <input type="text" class="form-control datetimepicker" disabled value={item.ngay}/>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="6">
                        <FormGroup>
                          <label>Địa điểm khám</label>
                          <Input
                          disabled
                            defaultValue={item.tenbenhvien}
                            placeholder="City"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="6">
                        <FormGroup>
                          <label>Bác sĩ thực hiện</label>
                          <Input
                          disabled
                            defaultValue={item.tenbacs}
                            placeholder="City"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
               
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Thông tin</label>
                          <Input
                          disabled
                            className="textarea"
                            type="textarea"
                            cols="80"
                            rows="4"
                            defaultValue={item.tendichvu}
                            
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                  <div className="submit">
                  <center><button type="button" class="btn btn-success"></button></center>   
                  </div>
                </CardBody>
                  ))
                }
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Chitietlichkham;

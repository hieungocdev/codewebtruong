import React from "react";

// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";
import Dslichsukhambenhs from '../../Api/Hosobacsi/Khambenh/Lichsukhambenh';
import { Link } from 'react-router-dom';

class Lichsukham extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: [],
    }
  }
  componentDidMount (){
    Dslichsukhambenhs()
   .then((resJson)=>{
     this.setState({
      data:resJson      
     })
   })
    .catch(err => console.log(err));
  }
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Lịch sử khám bệnh</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table responsive hover>
                    <thead className="text-primary">
                      <tr>
                        <th className="text-center">STT</th>
                        <th className="text-center">Tên bệnh nhân</th>
                        <th className="text-center">Ngày sinh</th>
                        <th className="text-center">Điện thoại</th>
                        <th className="text-center">Bác sĩ khám</th>
                        <th className="text-center">Giờ khám</th>
                        <th className="text-center">Nơi khám</th>
                        <th className="text-center">Quản lý</th>
                      </tr>
                    </thead>
                    <tbody>
           {
             this.state.data.map((item, index)=>(
              <tr>
                        <td className="text-center"></td>
                        <td className="text-center">{item.hovaten}</td>
                        <td className="text-center">{item.ngaysinh}</td>
                        <td className="text-center">{item.sodienthoai}</td>
                        <td className="text-center">{item.tenbacsi}</td>
                        <td className="text-center">{item.thoigian} {item.ngay}</td>
                        <td className="text-center">{item.tenbenhvien}</td>
                        <td className="text-center"> 
                        <Link to={`/admin/Chitietlichkham/${item.idhdxetnghiem}`}
                     className="btn-icon">
                      <i className="fa fa-edit" />
                    </Link> 
                          <Button
                            className="btn-icon"
                            color="danger"
                            id="tooltip4766097933"
                            size="sm"
                            type="button"
                          >
                            <i className="fa fa-times" />
                          </Button>{" "}
                          <UncontrolledTooltip
                            delay={0}
                            target="tooltip4766097933"
                          >
                            Xóa lịch khám
                          </UncontrolledTooltip>
                        </td>
                      </tr>
             ))
           }
                      
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Lichsukham;

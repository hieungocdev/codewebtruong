import React from "react";

// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";
import Danhsachbenhnhan from "../../Api/Hosobenhnhan/Danhsachbenhnhan";
import { Link } from 'react-router-dom';

class Dsbenhnhan extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: [],
    }
  }
  componentDidMount (){
    Danhsachbenhnhan ()
   .then((resJson)=>{
     this.setState({
      data:resJson      
     })
   })
    .catch(err => console.log(err));
  }
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Danh sách bệnh nhân</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table responsive hover>
                    <thead className="text-primary">
                      <tr>
                        <th className="text-center">STT</th>
                        <th className="text-center">Họ và tên</th>
                        <th className="text-center">Ngày sinh</th>
                        <th className="text-center">Điện thoại</th>
                        <th className="text-center">Email</th>
                        
                        <th className="text-center">Quản lý</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                     
                     
                    {
                      this.state.data.map((item, index)=>(
                        <tr>
                        <td className="text-center"></td>
                        <td className="text-center">{item.hovaten}</td>
                        <td className="text-center">{item.ngaysinh}</td>
                        <td className="text-center">{item.sodienthoai}</td>
                        <td className="text-center">{item.email}</td>
                 
                        <td className="text-center"> 
                        <Link to={`/admin/Chitietbenhnhan/${item.idnguoidung}`}
                     className="btn-icon">
                      <i className="fa fa-edit" />
                    </Link> 
                          <Button
                            className="btn-icon"
                            color="success"
                            id="tooltip3662466514"
                            size="sm"
                            type="button"
                          >
                            <i className="fa fa-edit" />
                          </Button>{" "}
                          <UncontrolledTooltip
                            delay={0}
                            target="tooltip3662466514"
                          >
                            Edit
                          </UncontrolledTooltip>
                          <Button
                            className="btn-icon"
                            color="danger"
                            id="tooltip4766097934"
                            size="sm"
                            type="button"
                          >
                            <i className="fa fa-times" />
                          </Button>{" "}
                          <UncontrolledTooltip
                            delay={0}
                            target="tooltip4766097934"
                          >
                            Delete
                          </UncontrolledTooltip>
                        </td>
                      </tr>
                      ))
                    }
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Dsbenhnhan;

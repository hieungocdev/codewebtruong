import React from "react";

// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  Form,
  UncontrolledTooltip
} from "reactstrap";

class Chitietbenhnhan extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tentintuc: "",
      hinhanh: "",
      noidung:"",
      tentintucState: "",
      hinhanhState: "",
      noidungState:"",
      alert_message: "",
      data:[],
      visible: true
    };
  }
// lay thong tin chi tiet benh
fetchData = async() =>{
const { match: { params } } = this.props;  
const response =  await fetch(`http://45.76.198.77/apiedoctor/admin/benhnhan.php?id=${params.id}`);
const products = await response.json(); 
this.setState({data: products}); 
};
componentDidMount(){
this.fetchData();
}
  render() {
    return (
      <>
        <div className="content">
          <center><h2 class="title">Hồ Sơ Bệnh Nhân</h2></center>
          {
            this.state.data.map((item, index)=>(
              <Row>
            <Col md="4">
              <Card className="card-user">
                <div className="image">
                  <img
                    alt="..."
                    // src={require("assets/img/bg/damir-bosnjak.jpg")}
                  />
                </div>
                <CardBody>
                  <div className="author">
                    <a href="#pablo" onClick={e => e.preventDefault()}>
                      <img
                        alt="..."
                        className="avatar border-gray"
                        // src={require("assets/img/benhnhan.jpg")}
                      />
                      <h5 className="title">BN. {item.hovaten}</h5>
                    </a>
                    <p className="description">@tvb123</p>
                  </div>
                
                </CardBody>
              </Card>
            </Col>
            <Col md="8">
              <Card>
                <CardHeader>
              <center><h5 className="title">Thông tin chung</h5></center>    
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col className="pr-1" md="7">
                        <FormGroup>
                        <label>Họ tên</label>
                          <Input
                            defaultValue={item.hovaten}
                            placeholder="Company"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-1" md="3">
                        <FormGroup>
                        <label>Ngày sinh</label>
                        <input type="text" class="form-control datetimepicker" value={item.ngaysinh}/>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="3">
                        <FormGroup>
                        <label>Chiều cao (cm)</label>
                          <Input
                            defaultValue={item.chieucao} cm
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pr-1" md="3">
                      <FormGroup>
                        <label>Cân nặng (kg)</label>
                          <Input
                            defaultValue={item.cannang}
                            type="text"
                          />
                        </FormGroup>
                      </Col> 
                     
                    </Row>
                    <Row>
                      <Col className="pr-1" md="5">
                        <FormGroup>
                        <label>Giới tính</label>
                          <Input
                            defaultValue={item.gioitinh}
                            
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-1" md="5">
                        <FormGroup>
                        <label>SĐT</label>
                        <Input
                            defaultValue={item.sodienthoai}
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                
                      <Col className="pl-1" md="6">
                        <FormGroup>
                        <label htmlFor="exampleInputEmail1">
                            Email
                          </label>
                          <Input value={item.email} type="email" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Địa chỉ</label>
                          <Input
                            defaultValue={item.diachi}
                            
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="6">
                        <FormGroup>
                          <label>Nghề nghiệp</label>
                          <Input
                            defaultValue={item.nghenghiep}
                        
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Tiền sử bệnh</label>
                          <Input
                            className="textarea"
                            type="textarea"
                            cols="80"
                            rows="4"
                            defaultValue={item.tinhtrangbenhly}
                           
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Yêu cầu bác sĩ</label>
                          <Input
                            className="textarea"
                            type="textarea"
                            cols="80"
                            rows="4"
                            defaultValue=""
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                  <div className="submit">
                  <center><button type="button" class="btn btn-success">Lưu thay đổi</button></center>   
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
            ))
          }
        </div>
      </>
    );
  }
}

export default Chitietbenhnhan;

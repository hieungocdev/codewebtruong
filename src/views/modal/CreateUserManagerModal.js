import React, { Component } from 'react';
import PictureUpload from 'components/CustomUpload/PictureUpload.jsx';
// reactstrap components
import {
    Row, Col, FormGroup, Form, Input

  } from "reactstrap";
class CreateUserManagerModal extends Component {
    constructor(props) {
        super(props);
        this.handleEdit = this.handleEdit.bind(this);
        this.state = {
                packagename: '',
                image: '',
                adsposted: '',
                view: '',
                expiry: '',
                price: ''
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
                packagename: nextProps.packagename,
                image: nextProps.image,
                adsposted: nextProps.adsposted,
                view: nextProps.view,
                expiry: nextProps.expiry,
                price: nextProps.price,
                status: nextProps.status
        });
    }

    packagenameHandler(e) {
        this.setState({ packagename: e.target.value });
    }

    imageHandler(e) {
        this.setState({ image: e.target.value });
    }

    adspostedHandler(e) {
        this.setState({ adsposted: e.target.value });
    }

    viewHandler(e) {
        this.setState({ view: e.target.value });
    }

    expiryHandler(e) {
        this.setState({ expiry: e.target.value });
    }

    priceHandler(e) {
        this.setState({ price: e.target.value });
    }

    handleEdit() {
        this.setState({ status: false });
        const item = this.state;
        this.props.editModalDetails(item)
    }

    handleAdd() {
        this.setState({
            status : false
        });
        const item = this.state;
        this.props.addModalDetails(item)
    }

    render() {
        return (
            <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header mr-auto ml-auto">
                            <h5 className="modal-title" id="exampleModalLabel">Edit License</h5>
                        </div>
                        <div className="modal-body">
                            <Form action="#" method="#">
                                <Row >
                                    <Col md='5' lg='5'>
                                        <label>Image :</label>
                                        <FormGroup>
                                            <PictureUpload value="hinh anh123"/>
                                        </FormGroup>
                                      
                                    </Col>
                                    <Col md='7' lg='7'>
                                        <label>Packagename :</label>
                                        
                                        <FormGroup>
                                            <Input  value={this.state.packagename} ref="packagename" onChange={(e) => this.packagenameHandler(e)} />
                                        </FormGroup>
                                        
                                        <label>Image :</label>
                                        <FormGroup>
                                            <Input value={this.state.image} ref="image" onChange={(e) => this.imageHandler(e)} />
                                        </FormGroup>

                                        <label>Adsposted :</label>
                                        <FormGroup>
                                            <Input value={this.state.adsposted} ref="adsposted" onChange={(e) => this.adspostedHandler(e)} />
                                        </FormGroup>

                                        <label>View :</label>
                                        <FormGroup>
                                            <Input value={this.state.view} ref="view" onChange={(e) => this.viewHandler(e)} />
                                        </FormGroup>

                                        <label>Expiry :</label>
                                        <FormGroup>
                                            <Input value={this.state.expiry} ref="expiry" onChange={(e) => this.expiryHandler(e)} />
                                        </FormGroup>

                                        <label>Price :</label>
                                        <FormGroup>
                                            <Input value={this.state.price} ref="price" onChange={(e) => this.priceHandler(e)} />
                                        </FormGroup>
                                    </Col>
                                </Row>
                            </Form>
                        </div>
                        <div className="modal-footer mr-auto ml-auto">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => { this.handleEdit() }}>Edit changes</button>
                            <button type="button" className="btn btn-info" data-dismiss="modal" onClick={() => { this.handleAdd() }}>Add changes</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreateUserManagerModal;
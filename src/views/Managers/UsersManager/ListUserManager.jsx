import React from "react";
// reactstrap components
import {
  Button, Card, CardHeader, CardBody,
  Table, Row, Col,
  UncontrolledTooltip,
  ButtonGroup,
  Modal, ModalBody, ModalFooter
} from "reactstrap";
// import api ListAdmin
import ListAdmin from '../../../Api/AdminManage/ListAdmin';
class ListUserManager extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      requiredItem: 0,
      brochure: [ ],
      currentPage: 1,
      todosPerPage: 3,
      // khai báo mảng chứa dữ liêu
      DataList:[],
      // modal poup hien thi tao quang cao
      modalCreateUserAdmins: false,
      DemoListData:[
        {
          "name":"dmeo1",
          "tuoi":"15",
          "truogn":"hiuec"
        },
        {
          "name":"dmeo2",
          "tuoi":"15",
          "truogn":"hiuec"
          
        },
        {
          "name":"dmeo3",
          "tuoi":"15",
          "truogn":"hiuec"
          
        },
        {
          "name":"dmeo4",
          "tuoi":"15",
          "truogn":"hiuec"
        },
          {
          "name":"dmeo4",
          "tuoi":"15",
          "truogn":"hiuec"
        },
        {
          "name":"dmeo4",
          "tuoi":"15",
          "truogn":"hiuec"
        },
        {
          "name":"dmeo4",
          "tuoi":"15",
          "truogn":"hiuec"
        },
        {
          "name":"dmeo4",
          "tuoi":"15",
          "truogn":"hiuec"
        },
        {
          "name":"dmeo4",
          "tuoi":"15",
          "truogn":"hiuec"
        },
        {
          "name":"dmeo4",
          "tuoi":"15",
          "truogn":"hiuec"
        },
        {
          "name":"dmeo4",
          "tuoi":"15",
          "truogn":"hiuec"
        },
      ]
    }
    this.handleClick = this.handleClick.bind(this);
  }
 
  componentDidMount (){
    ListAdmin()
   .then((resJson)=>{
     this.setState({
      brochure:resJson
     })
   })
    .catch(err => console.log(err));
  }
  // to stop the warning of calling setState of unmounted component
  componentWillUnmount() {
    var id = window.setTimeout(null, 0);
    while (id--) {
      window.clearTimeout(id);
    }
  }
  replaceModalItem = (modalID) => {
    this.setState({
      requiredItem: modalID
    });
  }
  addModalItem() {
    this.props.history.push("/admin/create-users-manager")
  }
  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }
  createUserAdmins= () =>{
    this.setState({
      modalCreateUserAdmins: !this.state.modalCreateUserAdmins
    });
}

  render() {
    const { DemoListData, currentPage, todosPerPage   } = this.state;
    // Logic for displaying todos
    var totalPage = Math.ceil(DemoListData.length / todosPerPage);
    var startPageVar  , endPageVar;
    
    if(totalPage <= 3)
    {
      startPageVar = 1;
      endPageVar = totalPage;
    }else {
      // more than 3 total pages so calculate start and end pages
      if (currentPage <= 2) {
        startPageVar = 1;
        endPageVar = 3;
      } else if (currentPage + 1 >= totalPage) {
        startPageVar = totalPage - 2;
        endPageVar = totalPage;
      } else {
        startPageVar = currentPage - 1;
        endPageVar= currentPage + 1;
      }
  }
  
  const indexOfLastTodo = (currentPage) * todosPerPage;
  const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
  const currentTodos = DemoListData.slice(indexOfFirstTodo, indexOfLastTodo);
  
    const renderTodos = currentTodos.map((todo, index) => {
      return (
        <tr key={index}>
          <td className="text-center">1</td>
          <td>{todo.name}</td>
          <td>Develop</td>
          <td className="text-center">2013</td>
          <td className="text-right">€ 99,225</td>
          <td className="text-right">
            <Button
              className="btn-icon" color="info"
              id="tooltip264453216"
              size="sm"
              type="button"
            >
              <i className="fa fa-user" />
            </Button>{" "}
            <UncontrolledTooltip
              delay={0}
              target="tooltip264453216"
            >
              Like
          </UncontrolledTooltip>
            <Button
              className="btn-icon" color="success"
              id="tooltip366246651"
              size="sm"
              type="button"
            >
              <i className="fa fa-edit" />
            </Button>{" "}
            <UncontrolledTooltip
              delay={0}
              target="tooltip366246651"
            >
              Edit
          </UncontrolledTooltip>
            <Button
              className="btn-icon" color="danger"
              id="tooltip476609793"
              size="sm"
              type="button"
            >
              <i className="fa fa-times" />
            </Button>{" "}
            <UncontrolledTooltip
              delay={0}
              target="tooltip476609793"
            >
              Delete
          </UncontrolledTooltip>
          </td>
        </tr>);
    });

    // Logic for displaying page numbers
    const pageNumbers = [];
    for (let i = startPageVar; i <= endPageVar; i++) {
      pageNumbers.push(i);
    }
    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <Button
          key={number} id={number}
          onClick={this.handleClick}
          className={currentPage === number ? "active btn-round  " : "btn-round "} 
          color="info" outline
        >
          {number}
        </Button>
      );
    });
    return (
      <>
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <h4 className="card-title float-left ml-4">User Manager</h4>
                  <div className="float-right mr-4">
                    <Button
                      className="btn-icon "
                      color="info"
                      id="tooltip264453217"
                      size="md" type="button"
                      data-toggle="modal" data-target="#exampleModal"
                      onClick={this.createUserAdmins}>
                      >
                      <i className="fas fa-plus rounded" />
                    </Button>{" "}
                    <UncontrolledTooltip delay={0}
                      target="tooltip264453217" >
                      Add
                          </UncontrolledTooltip>
                    
                  </div>
                </CardHeader>

                <CardBody>
                  <Table responsive>
                    <thead className="text-primary">
                      <tr>
                        <th className="text-center">ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th className="text-center">UserName</th>
                        <th className="text-right">Group</th>
                        <th className="text-right">Status</th>
                      </tr>
                    </thead>
                    <tbody>

                      {renderTodos}

                    </tbody>
                  </Table>
                  <div className="text-center">
                    <ButtonGroup className="Pagination">
                      <Button
                        key={this.state.currentPage - 1} id={this.state.currentPage - 1}
                        onClick={this.handleClick}
                        className={this.state.currentPage === 1 ? ' btn-round disabled' : 'btn-round'}
                        color="info" outline
                        type="button"
                      >
                        Prev
                      </Button>
                      {renderPageNumbers}
                      <Button
                        key={this.state.currentPage + 1} id={this.state.currentPage + 1}
                        onClick={this.handleClick}
                        className={this.state.currentPage === endPageVar ? ' btn-round disabled' : 'btn-round'}
                        color="info" outline
                        type="button"
                      >
                        Next
                      </Button>
                    </ButtonGroup>

                  </div>
                </CardBody>
              </Card>
            </Col>

          </Row>

         {/*  MODAL */}
          <Modal isOpen={this.state.modalCreateUserAdmins} toggle={this.togglemodalCreateUserAdmins}>
                      <div className="modal-header justify-content-center">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.togglemodalCreateUserAdmins}>
                          <span aria-hidden="true">×</span>
                        </button>
                        <h5 className="modal-title">Account information</h5>
                      </div>
                      <ModalBody>

                        <Row className="show-grid">
                          <Col xs={12} md={8}>
                            <code>.col-xs-12 .col-md-8</code>
                          </Col>
                          <Col xs={6} md={4}>
                            <code>.col-xs-6 .col-md-4</code>
                          </Col>
                        </Row>

                        <Row className="show-grid">
                          <Col xs={6} md={4}>
                            <code>.col-xs-6 .col-md-4</code>
                          </Col>
                          <Col xs={6} md={4}>
                            <code>.col-xs-6 .col-md-4</code>
                          </Col>
                          <Col xs={6} md={4}>
                            <code>.col-xs-6 .col-md-4</code>
                          </Col>
                        </Row>
                      </ModalBody>
                      <ModalFooter>
                        <Button color="secondary" onClick={this.togglemodalCreateUserAdmins}>
                          Close
                        </Button>
                        <Button color="primary">
                          Save changes
                        </Button>
                      </ModalFooter>
                    </Modal>
        </div>
      </>
    );
  }
}


export default ListUserManager;

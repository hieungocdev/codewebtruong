import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Label,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col,
  Row
} from "reactstrap";

class Login extends React.Component {
  componentDidMount() {
    document.body.classList.toggle("login-page");
  }
  componentWillUnmount() {
    document.body.classList.toggle("login-page");
  }
  constructor(props) {
    super(props);
    this.state = {


      // login form
      loginPhone: "",
      loginPassword: "",
      loginPhoneState: "",
      loginPasswordState: "",
      // type validation form
      required: "",
      phone: "",
      password: "",
      phoneState: "",
      passwordState: "",
      // range validation form

      range: "",
      min: "",
      max: ""
    };
  }
  // function that verifies if a string has a given length or not
  verifyLength = (value, length) => {
    if (value.length >= length) {
      return true;
    }
    return false;
  };
  // function that verifies if value contains only phone
  verifyPhone = value => {
    var phoneRex = new RegExp("^[0-9]+$");
    if (phoneRex.test(value)) {
      return true;
    }
    return false;
  };
  change = (event, stateName, type, stateNameEqualTo, maxValue) => {
    switch (type) {
      case "phone":
        if (this.verifyPhone(event.target.value)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "password":
        if (this.verifyLength(event.target.value, 1)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  };
  loginClick = () => {

    if (this.state.loginPasswordState === "") {
      this.setState({ loginPasswordState: "has-danger" });
    }
    if (this.state.loginPhoneState === "") {
      this.setState({ loginPhoneState: "has-danger" });
    }
    if (this.state.loginPhoneState === "has-success" && this.state.loginPasswordState === "has-success") {
      this.props.history.push('/admin/dashboard');
    }
  };
  registerClick = () => {
    this.props.history.push('/auth/register');
  };
  render() {
    // taking all the states
    let {
      // register form
      loginPhoneState,
      loginPasswordState
    } = this.state;
    return (
      <div className="login-page">
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" lg="4" md="6">
              <Form action="" className="form" method="">
                <Card className="card-login">
                  <CardHeader>
                    <CardHeader>
                      <h3 className="header text-center">Login</h3>
                    </CardHeader>
                  </CardHeader>
                  <CardBody>
                    <Form action="" className="form" method="">
                      <FormGroup className={`has-label ${loginPhoneState}`}>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="nc-icon nc-mobile" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input placeholder="Mobile phone..." name="phone" type="text" onChange={e => this.change(e, "loginPhone", "phone")} />
                        </InputGroup>
                        {this.state.loginPhoneState === "has-danger" ? (
                          <label className="error">
                            Please enter a valid Phone Number.
                        </label>
                        ) : null}
                      </FormGroup>
                      <FormGroup className={`has-label ${loginPasswordState}`}>
                        <InputGroup>
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="nc-icon nc-key-25" />
                            </InputGroupText>
                          </InputGroupAddon>
                          <Input placeholder="Password..." name="password" type="password"
                            autoComplete="off"
                            onChange={e => this.change(e, "loginPassword", "password")} />
                        </InputGroup>
                        {this.state.loginPasswordState === "has-danger" ? (
                          <label className="error">
                            Please enter a valid Password.
                        </label>
                        ) : null}
                      </FormGroup>
                      <br />
                      <FormGroup>
                        <FormGroup check>
                          <Label check>
                            <Input
                              defaultChecked
                              defaultValue=""
                              type="checkbox"
                            />
                            <span className="form-check-sign" />
                            Subscribe to newsletter
                        </Label>
                        </FormGroup>
                      </FormGroup>
                    </Form>
                  </CardBody>
                  <CardFooter>
                    <Button
                      block
                      className="btn-round mb-3"
                      color="info"
                      href="#"
                      onClick={this.loginClick}
                    >
                      Login
                    </Button>
                    <Button
                      block
                      className="btn-round mb-3"
                      color="primary"
                      href="#"
                      onClick={this.registerClick}
                    >
                      Don't have account?
                    </Button>
                  </CardFooter>
                </Card>
              </Form>
            </Col>
          </Row>
        </Container>
        <div
          className="full-page-background"
          style={{
            backgroundImage: `url(${require("assets/img/bg/login.jpg")})`,
            position:"absolute"
          }}
        />
      </div>
    );
  }
}

export default Login;

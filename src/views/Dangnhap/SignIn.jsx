import React from "react";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  FormGroup,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col,
  Row
} from "reactstrap";
import { Redirect } from "react-router-dom";
// api dang nhap
import DangNhap from '../../Api/dangnhap/Dangnhap';
//Hàm api đăng nhập

import NotificationAlert from "react-notification-alert";

class Login extends React.Component {
  componentDidMount() {
    document.body.classList.toggle("login-page");
  }

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      matkhau: "",
      emailState: "",
      matkhauState: "",
      alert_message: "",
      visible: true
    };
    this.onChange = this.onChange.bind(this);
  }

//hàm xác minh xem một chuỗi có độ dài
  verifyLength = (value, length) => {
    if (value.length >= length) {
      return true;
    }
    return false;
  };
  // Kiểm tra thông tin đăng nhập
  change = (event, stateName, type, stateNameEqualTo) => {
    switch (type) {
      case "matkhau":
        if (this.verifyLength(event.target.value, 1)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;
      case "length":
        if (this.verifyLength(event.target.value, stateNameEqualTo)) {
          this.setState({ [stateName + "State"]: "has-success" });
        } else {
          this.setState({ [stateName + "State"]: "has-danger" });
        }
        break;

      default:
        break;
    }
    this.setState({ [stateName]: event.target.value });
  };

  // Poup thông báo khi đăng nhập sai
  onFail = place => {
    var options = {};
    options = {
      place: place,
      message: (
        <div>
          <div>
            <b>Tên đăng nhập và mật khẩu không đúng</b>
          </div>
        </div>
      ),
      type: "danger",
      icon: "now-ui-icons ui-1_bell-53",
      autoDismiss: 7
    };
    this.refs.notificationAlert.notificationAlert(options);
  };



  Login = () => {
    const { email, matkhau } = this.state;

    if (this.state.emailState === "") {
      this.setState({ emailState: "has-danger" });
    }
    if (this.state.matkhauState === "") {
      this.setState({ matkhauState: "has-danger" });
    }
    if (
      this.state.emailState === "has-success" &&
      this.state.matkhauState === "has-success"
    ) {
      DangNhap(email, matkhau)
        .then(res => {
          console.log(res);
          if (res.message === "Invalid Username or Password") {
            this.onFail("tr");
          }
          else{
            // alert("dang nhap thanh cong")
            sessionStorage.setItem("id", res.user.id);
            sessionStorage.setItem("token", res.token);
            this.setState(

              //this.props.history.push("/admin/dashboard")
              <Redirect to={{ pathname: "/admin/dashboard" }}
              />
              );
          }
        })
    }
  };
  isAuthenticated() {
    const accessToken = sessionStorage.getItem("token");
    return accessToken && accessToken.length > 10;
  }
  // lấy các giá trị bàn phím
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  handleKeyPress(event) {

      if (event.key === '13' || event.which === 13) {
         document.getElementById("Login").click();
      }
    }
  render() {
    let {
      emailState,
      matkhauState
    } = this.state;
    const AuthRoute = this.isAuthenticated();
    return (
      <div className="login-page">
        <NotificationAlert ref="notificationAlert" />

        {AuthRoute ? (
          <Redirect to={{ pathname: "/admin/dashboard" }} />
        ) : (
          <Container>
            <Row>
              <Col className="ml-auto mr-auto" lg="4" md="6" sm="6">
                <div className="form">
                  <Card className="card-login">
                    <CardHeader>
                      <CardHeader>
                        <h3 className="header text-center">Login</h3>
                      </CardHeader>
                    </CardHeader>
                    <CardBody>
                      <div className="form" onKeyPress={this.handleKeyPress}>
                        <FormGroup className={`has-label ${emailState}`}>
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-email-85" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              type="text"
                              name="email"
                              onChange={e =>
                                this.change(e, "email", "length", 1)
                              }
                            />
                          </InputGroup>
                          {this.state.emailState === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                        <FormGroup className={`has-label ${matkhauState}`}>
                          <InputGroup>
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="nc-icon nc-key-25" />
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input
                              placeholder="Nhập mật khẩu..."
                              name="matkhau"
                              type="password"
                              onChange={e =>
                                this.change(e, "matkhau", "matkhau")
                              }
                            />
                          </InputGroup>
                          {this.state.matkhauState === "has-danger" ? (
                            <label className="error">
                              This field is required.
                            </label>
                          ) : null}
                        </FormGroup>
                      </div>
                    </CardBody>
                    <CardFooter>
                      <Button
                        className="btn-round mb-3"
                        color="info"
                        id="Login"
                        onClick={this.Login}
                      >
                        ĐĂNG NHẬP
                                              </Button>
                    </CardFooter>
                  </Card>
                </div>
              </Col>
            </Row>
          </Container>
        )}
        <div
          className="full-page-background"
          style={{
            backgroundImage: `url(${require("assets/img/bg/banner.jpg")})`,
            position: "absolute"
          }}
        />
      </div>
    );
  }
}
export default Login;

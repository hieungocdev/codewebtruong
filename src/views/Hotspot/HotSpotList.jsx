import React from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Col
} from "reactstrap";

const dataTable = [
  ["NDC1","4IPNET-60dNDC", "60 NGUYỄN ĐÌNH CHIỂU","20","10","545","ONLINE"],
  ["NDC2","4IPNET-61NDC", "13 NGUYỄN THỊ KHAI","20","10","545","OFFLINE"],
  ["NDC3","4IPNET-62NDC", "25 PHẠM VĂN ĐỒNG","20","10","545","ONLINE"],
  ["NDC4","4IPNET-63NDC", "15 PHAN VĂN HÂN","20","10","545","ONLINE"],
  ["NDC5","4IPNET-64NDC", "30 ĐINH BỘ LĨNH","20","10","545","ONLINE"],
  ["NDC6","4IPNET-64NDC", "10 NGUYỄN CÔNG TRỨ","20","10","545","ONLINE"],
  ["NDC","4IPNET-65NDC", "70 CỘNG HOÀ","20","10","545","ONLINE"],
  ["NDC","4IPNET-66NDC", "20 PHÚ NHUẬN","20","10","545","ONLINE"]
];

export default class HotSpotList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

      data: dataTable.map((prop, key) => {
        return {
          id: key,
          ID: prop[0],
          tenHotspot: prop[1],
          diaChi: prop[2],
          dangTruyCap: prop[3],
          luotXem: prop[4],
          luotTruyCap: prop[5],
          trangThai: prop[6],
          actions: (
            // we've added some custom button actions
            <div className="actions-right">
              {/* cấu hình hotspot */}
              <Button

              color="black"
                size="sm"
                className="btn-icon btn-link like"
              >
                <i className="fas fa-cog" />
              </Button>{" "}
              {/* chỉnh sửa hotspot */}
              <Button

              color="primary"
                size="sm"
                className="btn-icon btn-link like"
              >
                <i className="fas fa-pencil-alt" />
              </Button>{" "}
              {/* use this button to add a edit kind of action */}

              {/* xoa hotspot */}
              <Button
                onClick={() => {
                  var data = this.state.data;
                  data.find((o, i) => {
                    if (o.id === key) {
                      // here you should add some custom code so you can delete the data
                      // from this component and from your server as well
                      data.splice(i, 1);
                      console.log(data);
                      return true;
                    }
                    return false;
                  });
                  this.setState({ data: data });
                }}
                color="danger"
                size="sm"
                className="btn-icon btn-link remove"
              >
                <i className="fa fa-times" />
              </Button>{" "}
            </div>
          )
        };
      })
    };
  }
  CreateHotSpot = () => {
    this.props.history.push('/admin/createhotspot');

  };
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4" className="float-left ml-2">HOTSPOT LIST</CardTitle>
                  <Button
                    onClick={this.CreateHotSpot}
                    color="primary" size="Normal" className="float-right mr-2">ADD</Button>
                </CardHeader>
                <div className="button">
                </div>
                <CardBody>
                  <ReactTable
                    data={this.state.data}
                    filterable
                    columns={[
                      {
                        Header: "ID",
                        accessor: "ID",
                        maxWidth:60
                      },
                      {
                        Header: "Name hotspot",
                        accessor: "tenHotspot",
                        maxWidth:500

                      },
                      {
                        Header: "Network location",
                        accessor: "diaChi",
                        maxWidth:1000

                      },
                      {
                        Header: "Online",
                        accessor: "dangTruyCap",
                        maxWidth:100

                      },
                      {
                        Header: "Views",
                        accessor: "luotXem",
                        maxWidth:100
                      },
                      {
                        Header: "Access times",
                        accessor: "luotTruyCap",
                        maxWidth:100
                      },
                      {
                        Header: "Status",
                        accessor: "trangThai",
                        maxWidth:100

                      },

                      {
                        Header: "Action",
                        accessor: "actions",
                        sortable: false,
                        filterable: false,
                        maxWidth:150
                      }
                    ]}
                    defaultPageSize={10}
                    showPaginationTop
                    showPaginationBottom={false}
                    /*
                      You can choose between primary-pagination, info-pagination, success-pagination, warning-pagination, danger-pagination or none - which will make the pagination buttons gray
                    */
                    className="-striped -highlight primary-pagination"
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

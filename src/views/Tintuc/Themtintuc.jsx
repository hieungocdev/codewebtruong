import React from "react";
import CKEditor from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Label,
  FormGroup,
  Form,
  Input,
  FormText,
  Row,
  Col
} from "reactstrap";
import { Redirect } from "react-router-dom";
//import api them tin tuc
import Addtintuc from '../../Api/Tintuc/themoitintuc';
//import thu vien thong bao
import NotificationAlert from "react-notification-alert";

class Themtintuc extends React.Component {
  // phuong thuc khoi tao
  constructor(props) {
    super(props);
    this.state = {
      tentintuc: "",
      hinhanh: "",
      noidung:"",
      tentintucState: "",
      hinhanhState: "",
      noidungState:"",
      alert_message: "",
      visible: true
    };
    this.onChange = this.onChange.bind(this);
  }
  // lay gia tri nhap ban phim
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  handleKeyPress(event) {

    if (event.key === '13' || event.which === 13) {
       document.getElementById("Dangtin").click();
    }
  }
  //hàm xác minh xem một chuỗi có độ dài
  verifyLength = (value, length) => {
    if (value.length >= length) {
      return true;
    }
    return false;
  };

    // Kiểm tra thông tin dang tin tuc
    change = (event, stateName, type, stateNameEqualTo) => {
      switch (type) {
        case "length":
          if (this.verifyLength(event.target.value, stateNameEqualTo)) {
            this.setState({ [stateName + "State"]: "has-success" });
          } else {
            this.setState({ [stateName + "State"]: "has-danger" });
          }
          break;
  
        default:
          break;
      }
      this.setState({ [stateName]: event.target.value });
    };

    // Poup thông báo khi đăng nhập sai
    thanhcong = place => {
      var options = {};
      options = {
        place: place,
        message: (
          <div>
            <div>
              <b>Đăng tin thành công</b>
            </div>
          </div>
        ),
        type: "danger",
        icon: "now-ui-icons ui-1_bell-53",
        autoDismiss: 7
      };
      this.refs.notificationAlert.notificationAlert(options);
    };
  // hàm dăng tin tức lên server
  Dangtin = () => {
    const { tentintuc,hinhanh,noidung } = this.state;

    if (this.state.tentintucState === "") {
      this.setState({ tentintucState: "has-danger" });
    }
    if (this.state.hinhanhState === "") {
      this.setState({ hinhanhState : "has-danger" });
    }
    
    if (
      this.state.tentintucState === "has-success" &&
      this.state.hinhanhState === "has-success"
    ) {
      Addtintuc(tentintuc,hinhanh,noidung)
        .then(res => {
                    if (res.message === "dang tin thanh cong") {
                     
            this.setState(
              this.props.history.push('/admin/dstintuc')

              // // this.thanhcong('tr'),
              // // <Redirect to={{ pathname: "/admin/dstintuc" }}
              // />
              );
            }else{
            this.setState(
              // <Redirect to={{ pathname: "/admin/dstintuc" }}
              // />
              );
          }
        })
    }
  };
  handleEditorChange() {
		return ( event, editor ) => {
			this.setState( { noidung: editor.getData() } );
		}
	}

  render() {
    let {
      tentintucState,
      hinhanhState,
      noidungState
    } = this.state;
    return (
      <>
        <div className="content">
        <NotificationAlert ref="notificationAlert" />

          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Quản lý tin tức</CardTitle>
                </CardHeader>
                <CardBody>
                  <div  onKeyPress={this.handleKeyPress}>
                    <label>Tên tin tức</label>
                    <FormGroup className={`has-label ${tentintucState}`}>
                      <Input 
                      placeholder="Nhập tên tin tức" 
                      type="text"
                      name="tentintuc"
                              onChange={e =>
                                this.change(e, "tentintuc", "length", 1)
                              }
                      />
                        {this.state.tentintucState === "has-danger" ? (
                            <label className="error">
                              Vui lòng tên tin tức
                            </label>
                          ) : null}
                    </FormGroup>
                    <label>Hình ảnh</label>
                    <FormGroup className={`has-label ${hinhanhState}`}>
                      <Input 
                      
                    
                      type="text"
                      name="hinhanh"
                              onChange={e =>
                                this.change(e, "hinhanh", "length", 1)
                              }
                      />
                        {this.state.hinhanhState === "has-danger" ? (
                            <label className="error">
                              Vui lòng chọn hình ảnh
                            </label>
                          ) : null}
                    </FormGroup>
                    <label>Nội dung</label>
                    <FormGroup className={`has-label ${noidungState}`}>
<CKEditor
                    editor={ClassicEditor}
					data={this.state.content || ''}
					onChange={this.handleEditorChange()}
             
                />
                      
                    </FormGroup>
                  </div>
                </CardBody>
                <CardFooter>
          

                  <Button
                       className="btn-round"
                        color="info"
                        id="Dangtin"
                        onClick={this.Dangtin}
                      >
                        cập nhật
                                              </Button>
                </CardFooter>
              </Card>
            </Col>

         
   

    
          </Row>
        </div>
      </>
    );
  }
}

export default Themtintuc;


import React from "react";
// react component for creating dynamic tables
import ReactTable from "react-table";
import renderHTML from 'react-render-html';
import { Link } from 'react-router-dom';
//import api danh sach tin tuc
import Listdstintuc from '../../Api/Tintuc/Danhsachtintuc';
// reactstrap components
import {
  Badge,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  CardText,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";
export default class Danhsachtintuc extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      requiredItem: 0,
      datalisttintuc: [],
    }
  }
  componentDidMount (){
    Listdstintuc()
   .then((resJson)=>{
     this.setState({
      datalisttintuc:resJson
      
     })
   })
    .catch(err => console.log(err));
  }
  CreateHotSpot = () => {
    this.props.history.push('/admin/createhotspot');

  };

  render() {
    return (
      <>
      
      <div className="content">
          <Row>
            <Col className="text-center" lg="12" md="12">
              <Card className="card-tasks">
                <CardHeader>
                  <CardTitle tag="h4">Tin tức</CardTitle>
                  <h5 className="card-category">Quản lý tin tức</h5>
                </CardHeader>
                <CardBody>
                  <div className="table-full-width table-responsive">
                    <Table>
                      <tbody>
           {
             this.state.datalisttintuc.map((item,index)=>(
              <tr>
                     
                     <td className="img-row">
                       <div className="img-wrapper">
                         <img
                           alt="..."
                           className="img-raised"
                           src={`http://45.76.198.77/apiedoctor/images/tintuc/${item.hinhanh}`} 

                         />
                       </div>
                     </td>
                     <td className="text-left">
                      {item.tentintuc}
                     </td>
                     <td className="td-actions text-right">
                    <Link to={`/admin/chitiettintuc/${item.id}`}
                     className="btn-round btn-icon btn-icon-mini btn-neutral"
                    >
<i className="nc-icon nc-ruler-pencil" />
</Link>

                       <Button
                         className="btn-round btn-icon btn-icon-mini btn-neutral"
                         color="danger"
                         id="tooltip570363224"
                         title=""
                         type="button"
                       >
                         <i className="nc-icon nc-simple-remove" />
                       </Button>
                       <UncontrolledTooltip
                         delay={0}
                         target="tooltip570363224"
                       >
                         Xóa tin
                       </UncontrolledTooltip>
                     </td>
                   </tr>
                   
             ))
           }
                        
                        
                      </tbody>
                    </Table>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

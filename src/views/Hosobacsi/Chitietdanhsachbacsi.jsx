import React from "react";

// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  Form,
  UncontrolledTooltip
} from "reactstrap";

class Chitietdanhsachbacsi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tentintuc: "",
      hinhanh: "",
      noidung:"",
      tentintucState: "",
      hinhanhState: "",
      noidungState:"",
      alert_message: "",
      data:[],
      visible: true
    };
  }
// lay thong tin chi tiet benh
fetchData = async() =>{
const { match: { params } } = this.props;  
const response =  await fetch(`http://45.76.198.77/apiedoctor/admin/Hosobacsi/chitietlichsu.php?id=${params.id}`);
const products = await response.json(); 
this.setState({data: products}); 
};
componentDidMount(){
this.fetchData();
}
  render() {
    return (
      <>
        <div className="content">
          <center><h2 class="title">Hồ Sơ Bác Sĩ</h2></center>
        {
          this.state.data.map((item, index)=>(
              <Row>
            <Col md="4">
              <Card className="card-user">
                <div className="image">
                  <img
                    alt="..."
                    src={require("assets/img/bg/damir-bosnjak.jpg")}
                  />
                </div>
                <CardBody>
                  <div className="author">
                  <h5 className="title">BS. {item.tenbacsi}</h5>
                    <p className="description"></p>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col md="8">
              <Card>
                <CardHeader>
              <center><h5 className="title">Thông tin chung</h5></center>    
                </CardHeader>
                <CardBody>
                  <div>
                    <Row>
                      <Col className="pr-1" md="7">
                        <FormGroup>
                        <label>Họ tên</label>
                          <Input
                            defaultValue={item.tenbacsi}
                            
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-1" md="4">
                        <FormGroup>
                        <label>Ngày sinh</label>
                        <input type="text" class="form-control datetimepicker"  disabled value={item.ngaysinh}/>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="5">
                        <FormGroup>
                        <label>Giới tính</label>
                          <Input
                          disabled
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="px-1" md="5">
                        <FormGroup>
                        <label>SĐT</label>
                        <Input
                         disabled
                            defaultValue={item.sodienthoai}
                            
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="6">
                        <FormGroup>
                        <label>Tài khoản</label>
                          <Input
                          disabled
                            defaultValue={item.email}
                            
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                      <Col className="pl-1" md="6">
                        <FormGroup>
                        <label htmlFor="exampleInputEmail1">
                            Email
                          </label>
                          <Input placeholder="nguyenvana@gmail.com" type="email" />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Địa chỉ</label>
                          <Input
                          disabled
                            defaultValue=""
                          
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col className="pr-1" md="6">
                        <FormGroup>
                          <label>Đơn vị công tác</label>
                          <Input
                            defaultValue={item.tenbenhvien}
                            placeholder="City"
                            type="text"
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md="12">
                        <FormGroup>
                          <label>Thông tin bác sĩ</label>
                          <Input
                            className="textarea"
                            type="textarea"
                            cols="80"
                            rows="4"
                            defaultValue={item.thongtin}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
          ))
        }
        </div>
      </>
    );
  }
}

export default Chitietdanhsachbacsi;

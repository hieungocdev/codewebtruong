import React from "react";

// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";
import Dsbacsi from '../../Api/Hosobacsi/Dsbacsi';
import { Link } from 'react-router-dom';

class Danhsachbacsi extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: [],
    }
  }
  componentDidMount (){
    Dsbacsi()
   .then((resJson)=>{
     this.setState({
      data:resJson      
     })
   })
    .catch(err => console.log(err));
  }
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <CardTitle tag="h4">Danh sách bác sĩ</CardTitle>
                </CardHeader>
                <CardBody>
                  <Table responsive hover>
                    <thead className="text-primary">
                      <tr>
                        <th className="text-center">STT</th>
                        <th className="text-center">Họ và tên</th>
                        <th className="text-center">Ngày sinh</th>
                        <th className="text-center">Điện thoại</th>
                        <th className="text-center">Chuyên khoa</th>
                        <th className="text-center">Email</th>
                        <th className="text-center">Đơn vị</th>
                        <th className="text-center">Quản lý</th>
                      </tr>
                    </thead>
                    <tbody>
           
                   {
                     this.state.data.map((item, index)=>(
                      <tr>
                        <td className="text-center"></td>
                        <td className="text-center">{item.tenbacsi}</td>
                        <td className="text-center">{}</td>
                        <td className="text-center">{item.sodienthoai}</td>
                        <td className="text-center">{item.tenchuyenkhoa}</td>
                        <td className="text-center">{item.email}</td>
                        <td className="text-center">{item.tenbenhvien}</td>
                        <td className="text-center"> 
                        <Link to={`/admin/Chitietbacsi/${item.idbacsi}`}
                     className="btn-icon">
                      <i className="fa fa-edit" />
                    </Link> 
                          <Button
                            className="btn-icon"
                            color="success"
                            id="tooltip366246655"
                            size="sm"
                            type="button"
                          >
                            <i className="fa fa-edit" />
                          </Button>{" "}
                          <UncontrolledTooltip
                            delay={0}
                            target="tooltip366246655"
                          >
                            Edit
                          </UncontrolledTooltip>
                          <Button
                            className="btn-icon"
                            color="danger"
                            id="tooltip476609791"
                            size="sm"
                            type="button"
                          >
                            <i className="fa fa-times" />
                          </Button>{" "}
                          <UncontrolledTooltip
                            delay={0}
                            target="tooltip476609791"
                          >
                            Delete
                          </UncontrolledTooltip>
                        </td>
                      </tr>
                     
                     ))
                   }
                     
              
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </>
    );
  }
}

export default Danhsachbacsi;

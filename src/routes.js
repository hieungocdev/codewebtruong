import Buttons from "views/components/Buttons.jsx";
import Calendar from "views/Calendar.jsx";
import Charts from "views/Charts.jsx";
import Dashboard from "views/Dashboard.jsx";
import ExtendedTables from "views/tables/ExtendedTables.jsx";
import AdvertiseManager from "views/tables/AdvertiseManager.jsx";
import UserManager from "views/tables/UserManager.jsx";
import LicenseManager from "views/tables/LicenseManager.jsx";
import GoogleMaps from "views/maps/GoogleMaps.jsx";
import GridSystem from "views/components/GridSystem.jsx";
import Icons from "views/components/Icons.jsx";
import Login from "views/Dangnhap/SignIn";
import Notifications from "views/components/Notifications.jsx";
import Panels from "views/components/Panels.jsx";
import ReactTables from "views/tables/ReactTables.jsx";
import RegularTables from "views/tables/RegularTables.jsx";
import SweetAlert from "views/components/SweetAlert.jsx";
import Typography from "views/components/Typography.jsx";
import UserProfile from "views/UserProfile/UserProfile.jsx";
import VectorMap from "views/maps/VectorMap.jsx";
import Widgets from "views/Widgets.jsx";
import Wizard from "views/forms/Wizard.jsx";

import CapNhatThongTin from "views/HoSo/CapNhatThongTin.jsx";
import ListUserManager from "views/Managers/UsersManager/ListUserManager";
import CreateUserManager from "views/Managers/UsersManager/CreateUserManager";


import HotSpotList from "views/Hotspot/HotSpotList.jsx";
import CreateHotSpot from "views/Hotspot/CreateHotSpot.jsx";

// router tin tuc
import Danhsachtintuc from "views/Tintuc/Danhsachtintuc.jsx";
import Themtintuc from "views/Tintuc/Themtintuc";
import Chitiettintuc from "views/Tintuc/Chitiettintuc";
// kham benh
import Lichchuakham from "views/khambenh/Lichchuakham";
import Lichsukham from "views/khambenh/Lichsukham";
import Chitietlichkham from "views/khambenh/Chitietlichkham";
// bac si
import Danhsachbacsi from "views/Hosobacsi/Danhsachbacsi";
import Chitietdanhsachbacsi from "views/Hosobacsi/Chitietdanhsachbacsi";
// ho so benh nhan
import Dsbenhnhan from "views/Hosobenhnhan/Dsbenhnhan";
import Chitietbenhnhan from "views/Hosobenhnhan/Chitietbenhnhan"
const routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    layout: "/admin"
  },
// thông tin tin tức
{
  collapse: true,
  name: "Tin tức",
  icon: "nc-icon nc-single-copy-04",
  state: "tablesCollapse",
  views: [
    {
      path: "/dstintuc",
      name: "Danh sách",
      mini: "tt",
      component: Danhsachtintuc,
      layout: "/admin"
      
    },
    {
      path: "/themtintuc",
      name: "Thêm tin",
      mini: "Adtt",
      component:Themtintuc,
      layout: "/admin",
    },
    {
      path: "/Chitiettintuc/:id",
      name: "ctt",
      icon: "nc-icon nc-ruler-pencil",
      component: Chitiettintuc,
      layout: "/admin",
      invisible: true,
    },
  ]
},

// lich kham benh
{
  collapse: true,
  name: "Lịch khám bệnh",
  icon: "nc-icon nc-single-copy-04",
  state: "tablesCollapses",
  views: [
    {
      path: "/dslichchuakham",
      name: "Danh sách lịch chưa khám",
      mini: "tt",
      component: Lichchuakham,
      layout: "/admin"
      
    },
    {
      path: "/dslichdakham",
      name: "Danh sách lịch đã khám",
      mini: "tt",
      component: Lichsukham,
      layout: "/admin"
      
    },
    {
      path: "/Chitietlichkham/:id",
      name: "ctt",
      icon: "nc-icon nc-ruler-pencil",
      component: Chitietlichkham,
      layout: "/admin",
      invisible: true,
    },
  ]
},
// danh sách bác sĩ
{
  collapse: true,
  name: "Hồ sơ",
  icon: "nc-icon nc-single-copy-04",
  state: "tablesCollapsesssss",
  views: [
    {
      path: "/dsbacsi",
      name: "Danh sách bác sĩ",
      mini: "tt",
      component: Danhsachbacsi,
      layout: "/admin"
      
    },
    {
      path: "/Chitietbacsi/:id",
      name: "Thêm tin",
      mini: "Adtt",
      component:Chitietdanhsachbacsi,
      layout: "/admin",
      invisible: true,
    },
    {
      path: "/dsbenhnhan",
      name: "Danh sách bệnh nhân",
      mini: "tt",
      component: Dsbenhnhan,
      layout: "/admin"
    },
    {
      path: "/Chitietbenhnhan/:id",
      name: "Danh sách bệnh nhân",
      mini: "tt",
      component: Chitietbenhnhan,
      layout: "/admin",
      invisible: true,
    },
  ]
},



















  {
    path: "/login",
    name: "Setting",
    //mini: "S",
    component: Login,
    layout: "/auth",
    invisible: true
  },

  // route tin tức
  
];

export default routes;
